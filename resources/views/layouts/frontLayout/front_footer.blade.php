<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-shopper</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe1.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe2.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe3.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe4.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Menu</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="/">Trang chủ</a></li>
								<li><a href="/">Sản phẩm</a></li>
								<li><a href="{{ url('page/contact') }}">Liên hệ</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">T-Shirt</a></li>
								<li><a href="#">Mens</a></li>
								<li><a href="#">Womens</a></li>
								<li><a href="#">Gift Cards</a></li>
								<li><a href="#">Shoes</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="{{ url('page/terms-conditions') }}">Terms & Conditions</a></li>
								<li><a href="{{ url('page/privacy-policy') }}">Privacy Policy</a></li>
								<li><a href="{{ url('page/refund-policy') }}">Refund Policy</a></li>
								<li><a href="#">Billing System</a></li>
								<li><a href="#">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="{{ url('page/about-us') }}">About Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Get the most recent updates from <br />our site and be updated your self...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © <?php echo date('Y'); ?> <a href="https://vinsofts.com" target="_blank">Vinsofts - JSC</a>. All rights reserved.</p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->

	<div class="log-in">
	    <a href="tel:19000000" id="popup__toggle" onclick="return true;">
	    <div class="circlephone" style="transform-origin: center;"></div>
	    <div class="circle-fill" style="transform-origin: center;"></div>
	    <div class="img-circle" style="transform-origin: center;"><div class="img-circleblock" style="transform-origin: center;"></div></div>
	      
	</div>
	<style type="text/css">
		.panel_call {
    width:235px;
    height:75px;
    border-radius: 10px;
    display:none;
    background:#FF6E1F;
  bottom:40px;
  right:120px;
  position:fixed;
  text-decoration:none;
  
}
.panel_call p {
  margin:auto;
  
  color:white;
}
.info_call {
  margin-top: 10px;
  margin-left: 10px;
}
.info-call { 
  font-size: 200%;
  margin-top: -20px;
}

.info_call_number {
  margin-top: -10px;
  font-size: 120%;
}

#popup__toggle{bottom:25px;
  left:10px;
  position:fixed;}
.img-circle{background-color:#FF6E1F;
  box-sizing:content-box;
  -webkit-box-sizing:content-box;}
.circlephone{box-sizing:content-box;
  -webkit-box-sizing:content-box;
  border: 2px solid #FF6E1F;
  width:150px;
  height:150px;
  bottom:-25px;left:10px;
  position:absolute;
  -webkit-border-radius:100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  opacity: .5;
  -webkit-animation: circle-anim 2.4s infinite ease-in-out !important;
  -moz-animation: circle-anim 2.4s infinite ease-in-out !important;
  -ms-animation: circle-anim 2.4s infinite ease-in-out !important;-o-animation: circle-anim 2.4s infinite ease-in-out !important;
  animation: circle-anim 2.4s infinite ease-in-out !important;-webkit-transition: all .5s;
  -moz-transition: all .5s;
  -o-transition: all .5s;transition: all 0.5s;}
.circle-fill{box-sizing:content-box;
  -webkit-box-sizing:content-box;
  background-color:#FF6E1F;
  width:100px;height:100px;bottom:0px;right:35px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;-webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;-moz-animation: circle-fill-anim 2.3s infinite ease-in-out;-ms-animation: circle-fill-anim 2.3s infinite ease-in-out;-o-animation: circle-fill-anim 2.3s infinite ease-in-out;animation: circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
.img-circle{box-sizing:content-box;-webkit-box-sizing:content-box;width:72px;height:72px;bottom: 14px;left: 49px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;opacity: .7;}
.img-circleblock{box-sizing:content-box;-webkit-box-sizing:content-box;width:72px;height:72px;background-image:url(http://icons.iconarchive.com/icons/graphicloads/100-flat/64/phone-icon.png);background-position: center center;background-repeat:no-repeat;animation-name: tossing;-webkit-animation-name: tossing;animation-duration: 1.5s;-webkit-animation-duration: 1.5s;animation-iteration-count: infinite;-webkit-animation-iteration-count: infinite;}
.img-circle:hover{opacity: 1;}
@keyframes pulse {0% {transform: scale(0.9);opacity: 1;}
  50% {transform: scale(1); opacity: 1; }   
  100% {transform: scale(0.9);opacity: 1;}}
 @-webkit-keyframes pulse {0% {-webkit-transform: scale(0.95);opacity: 1;}
  50% {-webkit-transform: scale(1);opacity: 1;}   
  100% {-webkit-transform: scale(0.95);opacity: 1;}}
@keyframes tossing {
  0% {transform: rotate(-8deg);}
  50% {transform: rotate(8deg);}
  100% {transform: rotate(-8deg);}}
@-webkit-keyframes tossing {
  0% {-webkit-transform: rotate(-8deg);}
  50% {-webkit-transform: rotate(8deg);}
  100% {-webkit-transform: rotate(-8deg);}}
@-moz-keyframes circle-anim {
  0% {-moz-transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;-moz-opacity: .1;-webkit-opacity: .1;-o-opacity: .1;}
  30% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;-moz-opacity: .5;-webkit-opacity: .5;-o-opacity: .5;}
  100% {-moz-transform: rotate(0deg) scale(1) skew(1deg);opacity: .6;-moz-opacity: .6;-webkit-opacity: .6;-o-opacity: .1;}}
@-webkit-keyframes circle-anim {
  0% {-webkit-transform: rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity: .1;}
  30% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);-webkit-opacity: .5;}
  100% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);-webkit-opacity: .1;}}
@-o-keyframes circle-anim {
  0% {-o-transform: rotate(0deg) kscale(0.5) skew(1deg);-o-opacity: .1;}
  30% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);-o-opacity: .5;}
  100% {-o-transform: rotate(0deg) scale(1) skew(1deg);-o-opacity: .1;}}
@keyframes circle-anim {
  0% {transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;}
  30% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;}
  100% {transform: rotate(0deg) scale(1) skew(1deg);
opacity: .1;}}
@-moz-keyframes circle-fill-anim {
  0% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
  50% {-moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);opacity: .2;}
  100% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
@-webkit-keyframes circle-fill-anim {
  0% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;  }
  50% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;  }
  100% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
@-o-keyframes circle-fill-anim {
  0% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
  50% {-o-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
  100% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
@keyframes circle-fill-anim {
  0% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
  50% {transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
  100% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
	</style>
