<?php use App\Product; ?>
<form action="{{ url('/products-filter') }}" method="post">{{ csrf_field() }}
	@if(!empty($url))
	<input name="url" value="{{ $url }}" type="hidden">
	@endif 
	<div class="left-sidebar">
		<h2>Danh mục sản phẩm</h2>
		<div class="panel-group category-products" id="accordian">
			@foreach($categories as $cat)
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								{{$cat->name}}
							</a>
						</h4>
					</div>
					<div id="{{$cat->id}}" class="panel-collapse collapse">
						<div class="panel-body">
							<ul>
								@foreach($cat->categories as $subcat)
									<?php $productCount = Product::productCount($subcat->id); ?>
									@if($subcat->status==1)
									<li><a href="{{ asset('products/'.$subcat->url) }}">{{$subcat->name}} </a> ({{ $productCount }})</li>
									@endif
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		
	</div>
</form>