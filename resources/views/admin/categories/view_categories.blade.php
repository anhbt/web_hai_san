@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
{{--     <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Danh mục</a> <a href="#" class="current">Trang danh mục sản phẩm</a> </div> --}}
    <h1>Danh mục sản phẩm</h1>
    @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_error') !!}</strong>
            </div>
        @endif   
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_success') !!}</strong>
            </div>
        @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Danh mục sản phẩm</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID danh mục</th>
                  <th>Tên danh mục</th>
                  <th>Level</th>
                  <th>URL danh mục</th>
                  <th>Hành động</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($categories as $category)
                <tr class="gradeX">
                  <td class="center">{{ $category->id }}</td>
                  <td class="center">{{ $category->name }}</td>
                  <td class="center">{{ $category->parent_id }}</td>
                  <td class="center">{{ $category->url }}</td>
                  <td class="center">
                    <a href="{{ url('/admin/edit-category/'.$category->id) }}" class="btn btn-primary btn-mini">Sửa</a> 
                    <a id="delCat" href="{{ url('/admin/delete-category/'.$category->id) }}" rel="{{ $category->id }}" rel1="delete-category" class="btn btn-danger btn-mini deleteRecord">Xoá</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection